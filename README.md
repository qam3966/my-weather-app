# my-weather-app

## Project setup (download packages)
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Other libraries used:
	1. typescript
	2. vuex
	3. axios
	4. jest (unit tests)
	5. cypress (e2e tests)

### Descripton
Weather app consist of two pages. In first page user can search by city name.
Data is fetched by Openweathermap api's. 
In the second page user can see weather for full week.
